#include <stdio.h>
#include <stdlib.h>
void function(int n);
int main()
{
    int n;
    printf("Enter number of lines:");
    scanf("%d",&n);

    for(int i=1; i<=n; i++)
    {
        function(i);
        printf("\n");
    }

   return 0;
}
void function(int n)
{
    int i=n-(n-1);
    if(n==0)
        return;
    printf("%d",n);
    function(n-1);
}


